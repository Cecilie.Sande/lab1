package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        boolean playing = true;
        System.out.printf("Let's play round %d\n", roundCounter);
        while (playing) {
            System.out.print("Your choice (Rock/Paper/Scissors)?\n");
            String playerChoice = sc.nextLine();
            
            int computerChoiceIndex = (int) (Math.random() * (int) rpsChoices.size());
            String computerChoice = rpsChoices.get(computerChoiceIndex);

            int humanRoundScore = 0;
            int computerRoundScore = 0;

            boolean validPlayerChoice = false;
            for (String choice : rpsChoices) {
                if (choice.equals(playerChoice)) { // Check if two strings are identical
                    validPlayerChoice = true;
                }
            }
            if (validPlayerChoice == true) { // play the game
                if (playerChoice.equals("rock") & computerChoice.equals("paper")) {
                    computerScore++;
                    computerRoundScore++;
                }
                else if (playerChoice.equals("rock") & computerChoice.equals("scissors")) {
                    humanScore++;
                    humanRoundScore++;
                }
                else if (playerChoice.equals("paper") & computerChoice.equals("rock")) {
                    humanScore++;
                    humanRoundScore++;
                }
                else if (playerChoice.equals("paper") & computerChoice.equals("scissors")) {
                    computerScore++;
                    computerRoundScore++;
                }
                else if (playerChoice.equals("scissors") & computerChoice.equals("paper")) {
                    humanScore++;
                    humanRoundScore++;
                }
                else if (playerChoice.equals("scissors") & computerChoice.equals("rock")) {
                    computerScore++;
                    computerRoundScore++;
                }
                System.out.printf("Human chose %s, computer chose %s. ", playerChoice, computerChoice);
                if (humanRoundScore < computerRoundScore) {
                    System.out.print("Computer wins!\n"); // New line is not specified so string is added to previous string. 
                }
                else if (humanRoundScore > computerRoundScore) {
                    System.out.print("Human wins!\n");
                }
                else {
                    System.out.print("It's a tie!\n");
                }
                System.out.printf("Score: human %d, computer %d. \n", humanScore, computerScore);
                System.out.print("Do you wish to continue playing? (y/n)?\n");
                String askToPlayAgain = sc.nextLine();
                if (askToPlayAgain.equals("y")) {
                    roundCounter++;
                    System.out.printf("Let's play round %d\n", roundCounter);
                    continue;
                }
                else if (askToPlayAgain.equals("n")) {
                    break;
                }
            }
            else {
                System.out.printf("I do not understand %s. Could you try again?\n", playerChoice);
                continue;
            }
        } System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
